package test;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class TestServiceImpl implements TestService
{

	@Inject private PingTask pingTask;

	private ScheduledFuture<?> future;

	@Resource
	private ManagedScheduledExecutorService executorService;

	@Override
	public void initialize()
	{
		this.future = this.executorService.scheduleAtFixedRate( this.pingTask, 2000L, 250L,
				TimeUnit.MILLISECONDS );
	}

	@PostConstruct
	public void construct()
	{

	}

	@PreDestroy
	public void destroy()
	{
		if ( this.future != null )
		{
			this.future.cancel( true );
		}
	}

}
