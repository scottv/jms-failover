package test;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.jms.DeliveryMode;
import javax.jms.JMSContext;
import javax.jms.Queue;
import javax.transaction.Transactional;

@Dependent
public class PingTask implements Runnable
{

	private static final Logger LOG = Logger.getLogger( TestServiceImpl.class.getName() );

	@Inject private JMSContext jmsContext;

	@Resource( lookup = "java:/jms/queue/TestQueue" ) private Queue testQueue;

	private final AtomicLong counter = new AtomicLong();

	@Override
	@Transactional
	public void run()
	{
		try
		{
			long value = this.counter.incrementAndGet();
			String message = "TEST " + value;
			byte[] data = message.getBytes( StandardCharsets.UTF_8 );
			this.jmsContext.createProducer()
					.setDeliveryMode( DeliveryMode.PERSISTENT )
					.send( this.testQueue, data );
		}
		catch ( Exception e )
		{
			LOG.severe( e.toString() );
		}
	}

}
