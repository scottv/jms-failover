package test;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;

@ApplicationScoped
public class TestServiceImpl implements TestService
{

	@Resource( lookup = "java:/ConnectionFactory" )
	private ConnectionFactory connectionFactory;

	private TestMessageListener listener;

	@Resource( lookup = "java:/jms/queue/TestQueue" ) private Queue testQueue;

	@Override
	public void initialize()
	{
		this.listener = new TestMessageListener( this.connectionFactory, this.testQueue );
	}

	@PostConstruct
	public void construct()
	{

	}

	@PreDestroy
	public void destroy()
	{
		if ( this.listener != null )
		{
			this.listener.close();
		}
	}

}
