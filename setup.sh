#!/bin/bash
#

set -e

WILDFLY_VERSION=13.0.0.Final

cd jms-redelivery
mvn package
cd ..

cd jms-ping
mvn package
cd ..

mkdir wildfly-srv1
tar --strip-components=1 -C wildfly-srv1 -x -f wildfly-$WILDFLY_VERSION.tar.gz

mkdir wildfly-srv2
tar --strip-components=1 -C wildfly-srv2 -x -f wildfly-$WILDFLY_VERSION.tar.gz

./wildfly-srv1/bin/jboss-cli.sh --file=setup.jbc
./wildfly-srv2/bin/jboss-cli.sh --file=setup.jbc

# Deploy ping project only to the 2nd node
./wildfly-srv2/bin/jboss-cli.sh --file=deploy-ping.jbc
