## Synopsis

This project is a proof of concept for JMS failover with Wildfly and ActiveMQ Artemis.  It consists of a JMS listener deployment (jms-redelivery), a JMS producer deployment (jms-ping), and a collection of scripts to set up and manage the test.

## jms-redelivery

jms-redelivery is a Maven project intended to be deployed as an EAR.  At startup, TestMessageListener binds to java:/jms/queue/TestQueue and logs any messages it receives on that queue. It's intended to be deployed to both nodes in the cluster.

## jms-ping

jms-ping creates a producer on java:/jms/queue/TestQueue at startup and sends a message every 250ms.  It's intended to be deployed on only one node.

## setup.sh

A utility script to compile the test projects and unpack and configure Wildfly for 2 standalone nodes. This script will also deploy jms-redelivery to both nodes, and will deploy jms-ping to the *second* node.

## remove.sh

This script simply removes the two Wildfly directories created by setup.sh: wildfly-srv1 and wildfly-srv2.

## run1.sh

Starts the Wildfly instance in ./wildfly-srv1.

## run2.sh

Starts the Wildfly instance in ./wildfly-srv2 (with a port offset of 100).

## grep-log.sh

Simply greps server.log for both Wildfly instances and sorts by the counter printed out by jms-ping.  Note that wildfly-srv{1,2}/standalone/log/server.log must be truncated before each run or previous test runs
will show up in the grep.

## Running the test

1. First, clear out the server logs:

    truncate --size=0 wildfly-srv{1,2}/standalone/log/server.log

2. In two separate console windows, execute run1.sh and run2.sh.

3. Look up the PID of the java process running the *first* Wildfly node:

    ps aux | grep wildfly-srv1

4. Send a SIGSTOP to the *first* Wildfly node.

    kill -STOP 19713

5. Wait a little while and then stop the second node either gracefully (Ctrl+C) or just stop everything with killall -9 java.

6. Inspect the logs.

At this point there will be 2-3 messages missing from the output. By sending SIGSTOP to the first node we're simulating a network failure.  If we were to start up both nodes, these "lost" messages will be sent
to the first (now functioning again) node.  The behaviour I want is for the JMS subsystem to retry sending these messages to another consumer (the second node) rather than waiting indefinitely for the first node
to become available again.
