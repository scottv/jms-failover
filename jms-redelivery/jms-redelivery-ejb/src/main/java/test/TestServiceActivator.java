package test;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

@Singleton
@Startup
public class TestServiceActivator
{

	@Inject private TestService testService;

	@PostConstruct
	public void construct()
	{
		this.testService.initialize();
	}

}
