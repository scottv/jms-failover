package test;

import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.Topic;

public class TestMessageListener implements MessageListener, AutoCloseable
{

	private static final Logger LOG = Logger.getLogger( "TestMessageListener" );

	private final JMSContext context;
	private final JMSConsumer consumer;

	public TestMessageListener( ConnectionFactory connectionFactory, Queue destination )
	{
		this.context = connectionFactory.createContext( JMSContext.DUPS_OK_ACKNOWLEDGE );
		this.consumer = this.context.createConsumer( destination );
		this.consumer.setMessageListener( this );
		this.context.start();
	}

	public TestMessageListener( ConnectionFactory connectionFactory, Topic destination )
	{
		this.context = connectionFactory.createContext( JMSContext.DUPS_OK_ACKNOWLEDGE );
		this.consumer = this.context.createSharedDurableConsumer( destination, "mySubscription" );
		this.consumer.setMessageListener( this );
		this.context.start();
	}

	@Override
	public void onMessage( Message message )
	{
		try
		{
			message.acknowledge();
			String body = new String( message.getBody( byte[].class ), StandardCharsets.UTF_8 );
			StringBuilder s = new StringBuilder();
			s.append( body );
			s.append( "; JMSDeliveryMode=" );
			s.append( message.getJMSDeliveryMode() );
			s.append( " (" );
			switch ( message.getJMSDeliveryMode() )
			{
				case DeliveryMode.NON_PERSISTENT:
					s.append( "NON_PERSISTENT" );
					break;
				case DeliveryMode.PERSISTENT:
					s.append( "PERSISTENT" );
					break;
				default:
					s.append( "UNKNOWN" );
					break;
			}
			s.append( ")" );
			LOG.info( s.toString() );
		}
		catch ( JMSException e )
		{
			LOG.warning( e.toString() );
		}
	}

	@Override
	public void close()
	{
		this.context.stop();
		this.consumer.close();
		this.context.close();
	}

}
